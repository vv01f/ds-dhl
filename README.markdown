[DS](https://ds.ccc.de/) CSV for DHL sending / labeling with [CSV-upload](https://www.dhl.de/de/privatkunden/pakete-versenden/online-frankieren.html?type=ShoppingCartImport)

doc: https://doku.ccc.de/Datenschleuder-Versendung

* import data 
* validate for arbitrary limits by DHL
* assemble compatible file for upload
