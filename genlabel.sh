#!/usr/bin/env sh
c17default="office@ccc.de"

#set -x
c1="SEND_NAME1"
c2="SEND_NAME2"
c3="SEND_STREET"
c4="SEND_HOUSENUMBER"
c5="SEND_PLZ"
c6="SEND_CITY"
c7="SEND_COUNTRY"
c8="RECV_NAME1"
c9="RECV_NAME2"
c10="RECV_STREET"
c11="RECV_HOUSENUMBER"
c12="RECV_PLZ"
c13="RECV_CITY"
c14="RECV_COUNTRY"
c15="PRODUCT"
c16="COUPON"
c17="SEND_EMAIL"
l1=35
l2=35
l3=35
l4=5
l5=11
l6=35
l7=3
l8=35
l9=35
l10=35
l11=5
l12=11
l13=35
l14=3
l15=30
l16=30
l17=100
ua="Mozilla/5.0 (X11; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0"
# Zugangsdaten für das Mediawiki aus Konfiguratonsdatei bzw. ggf. 
auth=""
getAuthData() {
	user=""
	pass=""
	#dir=$(pwd)
	#cd ~
	test -f ds.conf && {
		user=$(cat ds.conf|grep -i "^user="|cut -d= -f2) #
		pass=$(cat ds.conf|grep -i "^pass="|cut -d= -f2) # 
	} || {
		which pass >/dev/null && {
			passfile=ccc/doku-htaccess
			passstore=~/.password-store/
			test -f $(echo ${passstore}${passfile}".gpg") || { >&2 echo "path for password manager does not match: "${passfile}; exit 1; }
			user=$(pass ${passfile} |grep -i "login"|head -1|rev|cut -d" " -f1|rev)
			pass=$(pass ${passfile} |head -1)
		} || { >&2 echo "tool missing: pass (unix password manager)"; exit 1; }
	}
	test -z "$user" && { >&2 echo "no auth data found."; return 1; }
	echo "${user}:${pass}@"
}
auth=$(getAuthData)

#~ {{#ask:
 #~ [[Chaostreff-Active::true]]
 #~ [[Chaostreff-Wants-Datenschleuder::true]]
 #~ [[Chaostreff-Delivery-Provided::true]]
 #~ |?Chaostreff-Delivery-Name = Name1
 #~ |?Chaostreff-Delivery-Remark = Name2
 #~ |?Chaostreff-Delivery-Address = Street
 #~ |?Chaostreff-Delivery-Housenumber = Housenumber
 #~ |?Chaostreff-Delivery-Postcode = PLZ
 #~ |?Chaostreff-Delivery-City = City
 #~ |?Chaostreff-Country = Country
 #~ |headers=show
 #~ |sort=Chaostreff-Delivery-City
 #~ |mainlabel=-
 #~ |headers=show
 #~ |searchlabel=CSV
 #~ |format=csv
 #~ |default=csv-query-error
 #~ |filename=dhl-ds-versendung.csv
 #~ |sep=;
 #~ |limit=100
#~ }}
# https://doku.ccc.de/index.php?title=Spezial:Ask&x=-5B-5BChaostreff-2DActive%3A%3Awahr-5D-5D-20-5B-5BChaostreff-2DWants-2DDatenschleuder%3A%3Awahr-5D-5D-20-5B-5BChaostreff-2DDelivery-2DProvided%3A%3Awahr-5D-5D%2F-3FChaostreff-2DDelivery-2DName%3DRECV-5FNAME1%2F-3FChaostreff-2DDelivery-2DRemark%3DRECV-5FNAME2%2F-3FChaostreff-2DDelivery-2DAddress%3DRECV-5FSTREET%2F-3FChaostreff-2DDelivery-2DHousenumber%3DRECV-5FHOUSENUMBER%2F-3FChaostreff-2DDelivery-2DPostcode%3DRECV-5FPLZ%2F-3FChaostreff-2DDelivery-2DCity%3DRECV-5FCITY%2F-3FChaostreff-2DCountry%3DRECV-5FCOUNTRY%2F-3FChaostreff-2DConsumed-2DDatenschleudern%3DAMOUNT-5FEA&mainlabel=-&limit=100&order=asc&sort=Chaostreff-Delivery-City&offset=0&format=csv&headers=show&searchlabel=CSV&default=csv-query-error&sep=%3B&filename=dhl-ds-versendung.csv
ua="Mozilla/5.0 (X11; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0"
## Empfänger der DS
url="https://${auth}doku.ccc.de/index.php?title=Spezial:Semantische_Suche&x=-5B-5BChaostreff-2DActive%3A%3Awahr-5D-5D-20-5B-5BChaostreff-2DWants-2DDatenschleuder%3A%3Awahr-5D-5D-20-5B-5BChaostreff-2DDelivery-2DProvided%3A%3Awahr-5D-5D%2F-3FChaostreff-2DDelivery-2DName%3DName1%2F-3FChaostreff-2DDelivery-2DRemark%3DName2%2F-3FChaostreff-2DDelivery-2DAddress%3DStreet%2F-3FChaostreff-2DDelivery-2DHousenumber%3DHousenumber%2F-3FChaostreff-2DDelivery-2DPostcode%3DPLZ%2F-3FChaostreff-2DDelivery-2DCity%3DCity%2F-3FChaostreff-2DCountry%3DCountry%2F-3FChaostreff-2DConsumed-2DDatenschleudern%3DAmount&mainlabel=-&limit=100&order=asc&sort=Chaostreff-Delivery-City&offset=0&format=csv&headers=show&searchlabel=CSV&default=csv-query-error&sep=%3B&filename=dhl-ds-versendung.csv"
#~ url="https://${auth}doku.ccc.de/index.php?title=Spezial:Semantische_Suche&x=-5B-5BChaostreff-2DActive%3A%3Awahr-5D-5D-20-5B-5BChaostreff-2DWants-2DDatenschleuder%3A%3Awahr-5D-5D-20-5B-5BChaostreff-2DDelivery-2DProvided%3A%3Awahr-5D-5D%2F-3FChaostreff-2DDelivery-2DName%3DName%2F-3FChaostreff-2DDelivery-2DRemark%3DVermerk%2F-3FChaostreff-2DDelivery-2DAddress%3DAdresse%2F-3FChaostreff-2DDelivery-2DHousenumber%3DHausnummer%2F-3FChaostreff-2DDelivery-2DPostcode%3DPLZ%2F-3FChaostreff-2DDelivery-2DCity%3DStadt%2F-3FChaostreff-2DCountry%3DLand%2F-3FChaostreff-2DConsumed-2DDatenschleudern%3DDS-2DBedarf&mainlabel=-&limit=50&order=asc&sort=Erfa-Delivery-City&offset=0&format=csv&headers=show&searchlabel=CSV&default=csv-query-error&sep=%3B&filename=ds-erfa-adressen.csv"
#~ url="https://${auth}doku.ccc.de/index.php?title=Spezial:Semantische_Suche&x=-5B-5BChaostreff-2DActive%3A%3Awahr-5D-5D-20-5B-5BChaostreff-2DWants-2DDatenschleuder%3A%3Awahr-5D-5D-20-5B-5BChaostreff-2DDelivery-2DProvided%3A%3Awahr-5D-5D%2F-3FChaostreff-2DDelivery-2DName%3DName1%2F-3FChaostreff-2DDelivery-2DRemark%3DName2%2F-3FChaostreff-2DDelivery-2DAddress%3DStra%C3%9Fe%2F-3FChaostreff-2DDelivery-2DHousenumber%3DHausnummer%2F-3FChaostreff-2DDelivery-2DPostcode%3DPLZ%2F-3FChaostreff-2DDelivery-2DCity%3DStadt%2F-3FChaostreff-2DCountry%3DLand&mainlabel=Erfa%2FChaostreff&limit=100&order=asc&sort=Erfa-Delivery-City&offset=0&format=csv&headers=show&searchlabel=CSV&default=csv-query-error&sep=%3B&filename=dhl-ds-versendung.csv"

fn="/tmp/dhl/dhl-"$(date +%Y%m%dT%H%M%S)".csv"

of=$(echo "$fn"|rev|cut -d'.' -f2-|rev)"-1.csv"
mkdir -p $(echo "${fn}"|rev|cut -d'/' -f2-|rev)
curl -vv -k -o "${of}" -A "${ua}" "${url}"

if="$of"
of=$(echo "$fn"|rev|cut -d'.' -f2-|rev)"-2.csv"
format="UTF-8" #$(file -i ${if}|cut -d':' -f2-|cut -d' ' -f2)
iconv -o "$of" -f "${format}" -t WINDOWS-1252//TRANSLIT "${if}"

if="$of"
of=$(echo "$fn"|rev|cut -d'.' -f2-|rev)"-3.csv"
echo "RECV_NAME1;RECV_NAME2;RECV_STREET;RECV_HOUSENUMBER;RECV_PLZ;RECV_CITY;RECV_COUNTRY;AMOUNT_EA" > "$of"
tail -n +2 "$if" >> "$of"

if="$of"
of=$(echo "$fn"|rev|cut -d'.' -f2-|rev)"-4.csv"
sed -e 's/$'"/`echo \\\r`/" "$if" > "$of"

#curl -o "${fn}" -k -A "${ua}" -vv "${url}"
#~ && {
	# ersetze Anführungszeichen, e.V. inkl. Spaces und generell zero width Spaces
	fn=$of
	sed -i -e 's/^"//g' -e 's/"$//g' -e 's/;"/;/g' -e 's/";/;/g' -e 's/[[:space:]]e\.[\u00A0\u202F[:space:]]*V\./\ e.V./g' -e 's/\uFEFF//g' -e 's/;\r$/;0\r/g' ${fn}
#~}
echo "resulting file: ${of}"
